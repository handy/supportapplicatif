package dbUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {

    private final static String URL = "jdbc:mysql/localhost:3306/supportApplicatif";
    private final static String USER = "userpop";
    private final static String PW = "userpop";

    private static Connection INSTANCE = null;

    static {
        try {
            INSTANCE = DriverManager.getConnection(URL, USER, PW);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getINSTANCE() {
        return INSTANCE;
    }
}
