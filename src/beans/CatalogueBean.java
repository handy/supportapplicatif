package beans;


import java.io.Serializable;
import java.util.Map;

public class CatalogueBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Map<String,ProduitBean> catalogue = new TreeMap<>();

    public Map<String, ProduitBean> getCatalogue() {
        return catalogue;
    }

    public CatalogueBean() {
        ProduitBean prod = new ProduitBean();
        prod.setId("1000");
        prod.setNom("JavaServer Pages");
        prod.setDescr("Learn how to develop a JSP based web application.");
        prod.setPrix(32.95f);
        catalogue.put("1000", prod);

        prod = new ProduitBean();
        prod.setId("2000");
        prod.setNom("Java Servlet Programming");
        prod.setDescr("Learn how to develop a servlet based web application.");
        prod.setPrix(32.95f);
        catalogue.put("2000", prod);

        prod = new ProduitBean();
        prod.setId("3000");
        prod.setNom("Java In a Nutshell");
        prod.setDescr("Learn the Java programming language.");
        prod.setPrix(32.95f);
        catalogue.put("3000", prod);
    }
}