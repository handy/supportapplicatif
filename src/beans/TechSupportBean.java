package beans;

import dbUtils.MyConnection;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TechSupportBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String email;
    private String software;
    private String os;
    private String problem;

    private String firstName;
    private String lastName;
    private String phoneNumber;

    private static String insertStatementStr =
            "INSERT INTO customers VALUES(?, ?, ?, ?,) ";

    private static String selectCustomerStr =
            "SELECT fname, lname, phone " +
                    "FROM customers " +
                    "WHERE LOWER(email) = ? ";

    private static String insertSupp_request_Str =
            "INSERT INTO supp_requests(email, software, os, problem) " +
                    "VALUES(?, ?, ?, ?)";

    public TechSupportBean() {
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getSoftware() {
        return software;
    }

    public String getOs() {
        return os;
    }

    public String getProblem() {
        return problem;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void registerSupportRequest() {

        try {
            PreparedStatement insertStatement =
                    MyConnection.getINSTANCE().prepareStatement(insertSupp_request_Str);

            insertStatement.setString(1, email.toLowerCase());
            insertStatement.setString(2, software);
            insertStatement.setString(3, os);
            insertStatement.setString(4, problem);

            insertStatement.executeQuery();

            PreparedStatement selectStatement =
                    MyConnection.getINSTANCE().prepareStatement(selectCustomerStr);

            selectStatement.setString(1, email);

            ResultSet rs = selectStatement.executeQuery();

            if (rs.next()) {
                System.out.println("Le client est enregistré");
                this.setFirstName(rs.getString("fname"));
                this.setLastName(rs.getString("lname"));
                this.setPhoneNumber(rs.getString("phone"));

                this.registered = true;
            } else {
                this.registered = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private boolean registered;

    public boolean isRegistered() {
        return registered;
    }

    public void registerCustomer() throws SQLException {

        PreparedStatement insertStatement = null;

        try {
            MyConnection.getINSTANCE().setAutoCommit(false);

            insertStatement = MyConnection.getINSTANCE().prepareStatement(insertStatementStr);

            insertStatement.setString(1, email.toLowerCase());
            insertStatement.setString(2, firstName);
            insertStatement.setString(3, lastName);
            insertStatement.setString(4, phoneNumber);

            insertStatement.executeUpdate();
            MyConnection.getINSTANCE().commit();

        } catch (SQLException e) {
            MyConnection.getINSTANCE().rollback();
            throw e;
        }

    }

    public static void main(String[] args) {
    }
}
