package beans;

public class ProduitBean {

    private String id;
    private String nom;
    private String descr;
    private double prix;

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }


}
