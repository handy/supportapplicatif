<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"
        import="beans.*" %>

<jsp:useBean id="techSupportBean"
             scope="session"
             type="beans.TechSupportBean"/>

<hr />
Utilisateur actuel :
<%-- A la boomer--%>
<jsp:getProperty name="techSupportBean" property="firstName"/>

<%-- à la millenium --%>
${sessionScope.techSupportBean.lastName}

<hr />
Entreprise XYZ, Service Client au  1.800.xyz.corp<br />

<c:remove scope="session" var="techSupportBean" />
