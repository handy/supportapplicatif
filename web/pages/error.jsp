<%@page contentType="text/html"
        pageEncoding="UTF-8"
        isErrorPage="true" %>

<html>
<head>
  <title>Entreprise XYZ , Service IT</title>
</head>
  <body>
    <h1>Support technique</h1>

    <p>Nous sommes désolé, une erreur s'est produite lors du traitement de votre requête.</p>

    <p>Le problème rencontré est <%= exception %>

  </body>
</html>
