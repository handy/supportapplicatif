<%@page contentType="text/html"
        pageEncoding="UTF-8"
        errorPage="error.jsp" 
        import="beans.*" %>

<%-- Localisation du composant Metier    --%>
<jsp:useBean id="techSupportBean"
             scope="session"
             class="beans.TechSupportBean">
</jsp:useBean>

<%-- Mise à jour des 3 champ lname, fname et phone du Composant Métier --%>
<jsp:setProperty name="techSupportBean"
                 property="*"/>

<%-- Regle metier : enregistrement client    --%>
   <% techSupportBean.registerCustomer(); %>

<%-- cinematique --%>
<jsp:forward page="response.jsp"/>