<%@taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core"%>


<%@page contentType="text/html"
        pageEncoding="UTF-8"
        errorPage="error.jsp" 
        import="beans.*"%>

<%--  Création et init. du composant Metier --%>
<jsp:useBean id="techSupportBean"
             scope="session"
             class="beans.TechSupportBean">
    <jsp:setProperty name="techSupportBean" 
                     property="*"/>
</jsp:useBean>

<%-- Regle metier : enregistrement incident   Helas encore du Java --%>
<% techSupportBean.registerSupportRequest();   %>

<%-- Regle metier : le client existe-il en BD?   (isRegistered() )--%>
<c:if test="${techSupportBean.registered }">
    <jsp:forward page="response.jsp"/>
</c:if>

<jsp:forward page="regForm.jsp"/>


<%--
<% if (techSupportBean.isRegistered()) {%>
             <jsp:forward page="response.jsp"/>
<% } %>
             <jsp:forward page="regForm.jsp"/>
--%>