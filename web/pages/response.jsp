<%@page contentType="text/html"
        pageEncoding="UTF-8"
        errorPage="error.jsp" %>

<html>
    <head>
        <title> Réponse du Service Client</title>
    </head>
    <body>
        <h1> Demande de support technique reçue</h1>

        <p>Merci de votre demande.<p>
        <p>Celle-ci a été enregistrée. Une réponse sera apportée dans les trois jours ouvrables.</p>

        <jsp:include page="banner.jsp" />

    </body>
</html>
